## 阅读材料五：Scratch 中的三种程序结构
---
<br>

在 Scratch 虽然只是面向少年儿童的“积木式”的程序设计语言，但却能像其它计算机语言一样编写出顺序、循环、选择这三种基本结构组成的脚本。三种基本结构的脚本之间相互配合，共同实现具有复杂功能的程序功能。

<br>

### 一、顺序结构

顺序结构的 Scratch 脚本由多个积木顺次拼接而成，并按从上往下的顺序依次执行。即，先执行积木 A，再执行积木 B。

<div align=center><img src="img/15-1.png"/></div>

例如，在“喵喵背古诗”作品中，喵喵背诵《山行》的脚本就是按照先后顺序一个个积木依次执行。

<div align=center><img src="img/15-2.png"/></div>

<a href="http://haohaodada.com/video/a21501.php"  target="_blank" >单击此处</a>可以观看以上内容相关的视频。

<br>

### 二、循环结构

循环结构的 Scratch 脚本，在执行过程中能够重复执行同一段脚本，这段脚本被称为“循环体”。可以为循环结构的脚本设置一个循环条件，也就是当条件成立时才执行循环体内的积木，当条件不成立时则向下执行后续积木。

如果“顺序结构”的脚本中存在呈周期性重复出现的积木，就可以使用循环结构来进行简化，可以大大减少脚本编写的工作量。

<div align=center><img src="img/15-3.png"/></div>

Scratch 提供 ![img](block/5-3.png)、![img](block/5-2.png)、![img](block/5-7.png) 三个循环积木。

在上述积木中，![img](block/5-3.png) 积木没有循环结束条件，会一直执行循环体；

而![img](block/5-2.png) 和![img](block/5-7.png) 积木则由参数来确定循环条件，指定重复的次数或者重复的条件。

<a href="http://haohaodada.com/video/a21502.php"  target="_blank" >单击此处</a>可以观看以上内容相关的视频。

<br>

### 三、选择结构

选择结构的 Scratch 脚本，将根据给定的条件是否来成立，来决定是否执行相应的脚本。选择结构分为单选择结构和双选择结构。

<div align=center><img width ='300' src="img/15-4.png"/></div>

在 Scratch 中， ![img](block/5-4.png) 和 ![img](block/5-5.png) 积木分别用于实现单选择结构和双选择结构。在“喵喵学画画”作品中，就使用了双选择结构程序脚本，实现鼠标左键控制抬笔和落笔功能。

<a href="http://haohaodada.com/video/a21503.php" target="_blank" >单击此处</a>可以观看以上内容相关的视频。
