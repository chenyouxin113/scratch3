# Scratch 3 编程入门

本课程根据蒋先华老师主编的、由浙江教育出版社出版的《Scratch编程入门》一书改编而成，是专为小学中低年级学生学习Scratch 3 编程而编写的入门教材。

## 目录
* <a href="01.md" target="_blank"> 第1课 喵喵初体验 </a>
    * <a href="a.md" target="_blank"> 阅读材料一：Scratch 在线版的使用 </a>
* <a href="02.md" target="_blank"> 第2课 喵喵背古诗 </a>
    * <a href="b.md" target="_blank"> 阅读材料二：Scratch 离线版软件的安装及使用 </a>
* <a href="03.md" target="_blank"> 第3课 喵喵游西湖 </a>
* <a href="04.md" target="_blank"> 第4课 喵喵学做操 </a>
* <a href="05.md" target="_blank"> 第5课 喵喵学英语 </a>
    * <a href="c.md" target="_blank"> 阅读材料三：使用PowerPoint制作中文文字角色 </a>
* <a href="06.md" target="_blank"> 第6课 喵喵演奏会 </a>
* <a href="07.md" target="_blank"> 第7课 喵喵大抽奖 </a>
    * <a href="d.md" target="_blank"> 阅读材料四：Scratch中的指令类型 </a>
* <a href="08.md" target="_blank"> 第8课 喵喵运动会 </a>
* <a href="09.md" target="_blank"> 第9课 喵喵学画画 </a>
    * <a href="e.md" target="_blank"> 阅读材料五：Scratch中的三种程序结构诗 </a>
* <a href="10.md" target="_blank"> 第10课 喵喵出题目 </a>


## 版权申明

本课程是开源课程，仅供非盈利情况下免费使用。如要用于商业用途，请事先与蒋先华老师联系（<hzjxh@qq.com>）。


## 参与课程改编者  

* 总体协调：蒋先华、周娇蓉、何聪翀、张越英、鲁亭
* 文档规范：楼韵佳
* 参与改编：
    * 第1课：过映红
        * 阅读材料一：王胜男
    * 第2课：张自任
        * 阅读材料二：胡海林
    * 第3课：李孟婷
    * 第4课：王京京
    * 第5课：余许红
        * 阅读材料三：姚雪怡
    * 第6课：林紫秀
    * 第7课：朱珊珊
        * 阅读材料四：章贵宝
    * 第8课：姚春秋
    * 第9课：许一洲
    * 第10课：苗仁龙
        * 阅读材料五：鑫蕾
